
import React from "react";
import ReactDom from "react-dom";
import "./styles.css";

class App extends React.Component {
    state = {
        x: 0, // начальная позиция по X
        y: 0, // начальная позиция по Y
        dx: 5, // скорость по X
        dy: 5, // скорость по Y
    };

    componentDidMount() {
        // Запускаем анимацию после монтирования компонента
        this.animate();
    }

    animate = () => {
        const { x, y, dx, dy } = this.state;

        // Обновляем позицию
        const newX = x + dx;
        const newY = y + dy;

        // Проверяем, не достигла ли фигурка границ экрана
        if (newX < 0 || newX > window.innerWidth - 80) {
            this.setState({ dx: -dx }); // меняем направление по X
        }
        if (newY < 0 || newY > window.innerHeight - 80) {
            this.setState({ dy: -dy }); // меняем направление по Y
        }

        // Обновляем состояние с новыми координатами
        this.setState({ x: newX, y: newY });

        // Запускаем следующий кадр анимации
        requestAnimationFrame(this.animate);
    };

    render() {
        const { x, y } = this.state;

        return <div className="figure" style={{ top: y, left: x }} />;
    }
}
export default App;
ReactDom.render(<App />, document.getElementById('react'));

