INSERT INTO roles (id, name) VALUES (1, 'User')
ON CONFLICT (id) DO NOTHING;
INSERT INTO roles (id, name) VALUES (2, 'Admin')
ON CONFLICT (id) DO NOTHING;