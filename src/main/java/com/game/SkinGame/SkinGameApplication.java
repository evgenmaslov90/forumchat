package com.game.SkinGame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkinGameApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkinGameApplication.class, args);
	}

}
