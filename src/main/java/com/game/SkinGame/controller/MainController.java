package com.game.SkinGame.controller;

import com.game.SkinGame.model.entity.Message;
import com.game.SkinGame.repository.MessageRepository;
import com.game.SkinGame.repository.UserRepository;
import com.sun.security.auth.PrincipalComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    MessageRepository messageRepository;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {

        return "greeting";
    }

    @GetMapping("/main")
    public String main(Map<String, Object> model) {
        Iterable<Message> messages = messageRepository.findAll();
        model.put("messages", messages);
        return "main";
    }

    @PostMapping("/main")
    public String add(Principal principal, @RequestParam String text, Map<String, Object> model) {
        Message message = new Message(text, principal.getName());


        if(userRepository.findByUsername(principal.getName()).isActive()) {
            messageRepository.save(message);
        } 

        Iterable<Message> messages = messageRepository.findAll();

        model.put("messages", messages);

        int count = 0;

        for (Message m : messages) {

            count++;
            if (count > 17) {
                messageRepository.deleteAll();
                break;
            }
        }

        return "main";
    }

    @PostMapping("/filter")
    public String filter( String filter, Map<String, Object> model) {
        Iterable<Message> messages;

        if (filter != null && !filter.isEmpty()) {
            messages = messageRepository.findByName(filter);
        } else {
            messages = messageRepository.findAll();
        }

        model.put("messages", messages);

        return "main";
    }

    @GetMapping("/login")
    public String login() {
        return "login"; // Здесь "login" - имя вашего шаблона страницы логина
    }


}