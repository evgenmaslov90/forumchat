package com.game.SkinGame.controller;

import com.game.SkinGame.model.entity.Message;
import com.game.SkinGame.model.entity.User;
import com.game.SkinGame.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Map;

@Controller
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/admin")
    public String adminPanel(Map<String, Object> model) {
        Iterable<User> users = userRepository.findAll();
        model.put("users", users);
        return "admin";
    }

    @PostMapping("/admin")
    public String changeStatus(@RequestParam String name, @RequestParam boolean status, Map<String, Object> model) {

        User userInWorking = userRepository.findByUsername(name);

        userInWorking.setActive(status);

        userRepository.save(userInWorking);

        Iterable<User> users = userRepository.findAll();
        model.put("users", users);

        return "admin";
    }
}
