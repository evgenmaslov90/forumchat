package com.game.SkinGame.repository;

import com.game.SkinGame.model.entity.Message;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message, Long> {

    List<Message> findByName(String name);
}
